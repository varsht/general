# General

This repository is for saving my generally used files.

## Auto Install Docker

To use the auto install docker script
1. Get the raw script (or download or whatever suites you)
```shell
wget <raw link of the script>
```
2. Give the script executable permission
```shell
chmod +x docker-install-auto.sh
```
3. Run the script with root privileges. If you are not root then use sudo.
```shell
sudo ./docker-install-auto.sh
```
4. If you want to give your user the rights to use docker without root privileges then add your user to the command.
```shell
sudo ./docker-install-auto.sh -u $USER #OR --user=$USER for the user you are installing from
sudo ./docker-install-auto.sh -u <user-name> #for the user you want to give permission to
```
5. If you added your user to the command, logout and then log back in for it to take effect.
6. Run the following to see if everything works out.
```shell
docker run hello-world #from the user you gave permission to
sudo docker run hello-world #run docker as root
```

## Using the api-responder

This is an image file for an api-responder. The task for a container with this image is to listen on port 5000 for api request. This is handy to have if you want to have a quick listner server for testing api calls.

### Points to note
- To edit the behaviour of api calls, edit the app.py file.
- If you require any python packages that are not installed, list them in requirements.txt
- To keep the created image small, alpine version of the python image is used. If your packages are not supported by alpine then feel free to change the version of the image.

To use the files there are three steps.
1. Download the files to a directory.
```shell
mkdir api-responder # Could be any name
cd api-responder
wget <raw link of the directory>/.
```
2. Build the image from the directory
```shell
docker build -t api-resopnder . #Here api-responder is the name of the image you want to use. Feel free to use any name.
```
3. Use the image as you please
```shell
docker run --rm -p 5000:5000 --name listener-1 api-responder
```

## Using locust-loadtest

This is an image file for a locust loadtest tester. The task for a container with this image is to send api requests to the target server to load test it. This is handy to do a quick test and verify your locust file.

### Points to note
- To change the target server address, edit the Dockerfile.
- Change the content of locustfile.py to adapt to your testing scenario.

To use the files
1. Download the files to a directory.
```shell
mkdir locust-loadtest # Could be any name
cd locust-loadtest
wget <raw link of the directory>/.
```
2. Build the image from the directory
```shell
docker build -t locust-loadtest . #Here locust-loadtest is the name of the image you want to use. Feel free to use any name.
```
3. Use the image as you please
   - If the api responding application is a docker container running on the same machine as the tester container
     ```shell
     docker run --rm -p 8089:8089 --name tester-1 --link listner-1:target_server locust-loadtest
     ```
     `--link` in docker creates a host entery in the created container with the IP address of the linked container.
     Do make sure that the to be linked container is already running.
     You can choose whichever name you like. If you don't specify any target name, by default an entry with the name of the linked container is always made.
     To see this in effect see the host file in the container to understand it.
 
   - If the respondig server is somewhere else. Use the following
     ```shell
     # Run as single node
     docker run --rm -p 8089:8089 --name tester-1 --add-host=target_server:<IP address of the target host> locust-loadtest

     # Run as master
     docker run --rm -p 8089:8089 -p 5557:5557 --name tester-1 --add-host=target_server:<IP address of the target host> locust-loadtest
     
     # Run as a worker
     docker run --rm --name worker-1 --add-host=target_server:<IP address of the target host> --add-host=master:<IP address of the master> locust-loadtest
     ```
     - `--add-host` adds a static host entry to the newly created docker container.
     - Both `target_server` and `master` are required for the worker to work properly. Master will not pass the IP, only the hostname.
4. Now open a browser with `http://<IP address>:8089` and use locust.
   - IP address is `localhost` or IP of the docker container if running on the same machine.
   - IP address is of the machine if accessing from the network.
