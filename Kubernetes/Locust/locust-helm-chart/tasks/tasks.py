from locust import HttpUser, task, between

with open('urls') as my_file:
    url_array = [url[:-1] for url in my_file]

class MyUser(HttpUser):
    wait_time = between(5000, 9000)
    #host = "http://127.0.0.1:5000"
    
    @task(1)
    def Nicholas(self):
        self.client.get(url_array[0])

    @task(1)
    def Elvin(self):
        self.client.get(url_array[1])

    @task(1)
    def Jass(self):
        self.client.get(url_array[2])
