#!/bin/bash

##############################
# Input command manipulation
##############################

# Setting command line flag options to take as input
TEMP=`getopt -o u: --long user: -- "$@"`
# This line checks whether the options that require an argument have it or not
test $? -ne 0 && echo "Terminating..." >&2 && exit 1
# Setting it back to original input string
eval set -- "$TEMP"

# Setting local variables
User_to_access=

# Using the command line input to set user
while true; do
	case "$1" in
		-u | --user ) User_to_access="$2"; shift 2 ;;
		-- ) shift; break ;;
		* ) break ;;
	esac
done

#####################
# Installing Docker
#####################

# Update Repository
apt-get update

# Update the apt package index and install packages to allow apt to use a repository over HTTPS:
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
	-y

# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# Set up stable repository of docker
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update the apt package index, and install the latest version of Docker Engine and containerd,
# or go to the next step to install a specific version:
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io -y

# Add user to docker group to run docker as non-root user
test $User_to_access \
	&& usermod -aG docker $User_to_access
