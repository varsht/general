from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(5, 15)
    #host = "http://127.0.0.1:5000"
    
    @task(1)
    def Nicholas(self):
        self.client.get("/user/Nicholas")

    @task(1)
    def Elvin(self):
        self.client.get("/user/Elvin")

    @task(1)
    def Jass(self):
        self.client.get("/user/Jass")
